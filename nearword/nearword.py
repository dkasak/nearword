import argparse
import nearword


def levenshtein(source: str, target: str) -> int:
    import numpy as np
    if len(source) < len(target):
        return levenshtein(target, source)

    # So now we have len(source) >= len(target).
    if len(target) == 0:
        return len(source)

    # We call tuple() to force strings to be used as sequences
    # ('c', 'a', 't', 's') - numpy uses them as values by default.
    source_array = np.array(tuple(source))
    target_array = np.array(tuple(target))

    # We use a dynamic programming algorithm, but with the
    # added optimization that we only need the last two rows
    # of the matrix.
    previous_row = np.arange(target_array.size + 1)
    for s in source_array:
        # Insertion (target_array grows longer than source_array):
        current_row = previous_row + 1

        # Substitution or matching:
        # target_array and source_array items are aligned, and either
        # are different (cost of 1), or are the same (cost of 0).
        current_row[1:] = np.minimum(
            current_row[1:],
            np.add(previous_row[:-1], target_array != s))

        # Deletion (target_array grows shorter than source_array):
        current_row[1:] = np.minimum(
            current_row[1:],
            current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]


def main():
    parser = argparse.ArgumentParser(
        prog='nearword',
        description='Searches for words similar to a target word based on '
                    'Levenshtein distance. The wordlist should contain one '
                    'word per line.')
    parser.add_argument('target',
                        type=str,
                        help='target word to search similar words for')
    parser.add_argument('-d',
                        '--distance',
                        metavar='N',
                        default=1,
                        type=int,
                        help='maximum distance from word')
    parser.add_argument('-w',
                        '--wordlist',
                        metavar='PATH',
                        default=nearword.get_wordlist('croatian'),
                        type=open,
                        help='wordlist to use')

    args = parser.parse_args()

    words = args.wordlist.read().split()

    for word in words:
        if levenshtein(args.target, word) <= args.distance:
            print(word)


if __name__ == "__main__":
    main()
