import os

_ROOT = os.path.abspath(os.path.dirname(__file__))


def get_wordlist(w):
    return os.path.join(_ROOT, 'wordlists', w)
