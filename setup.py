from setuptools import setup
# from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='nearword',
    version='1.0.0',
    description=('Searches for words similar to a target word based on '
                 'Levenshtein distance. The wordlist should contain one word '
                 'per line.'),
    author='Denis Kasak',
    author_email='dkasak@termina.org.uk',
    license='GPL',
    classifiers=[
        'Development Status :: 3 - Beta',
        'License :: OSI Approved :: GPL License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='language',
    packages=['nearword'],

    package_data={
        'nearword': ['wordlists/*'],
    },
    entry_points={
        'console_scripts': [
            'nearword=nearword.nearword:main',
        ],
    },
)
